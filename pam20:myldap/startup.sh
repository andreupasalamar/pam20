#! /bin/bash

# afegim usuaris
useradd unix01
useradd unix02
useradd unix03
echo -e "unix01\nunix01\n" | passwd unix01 --stdin
echo -e "unix02\nunix02\n" | passwd unix02 --stdin
echo -e "unix03\nunix03\n" | passwd unix03 --stdin

# Serveis
mv /etc/openldap/ldap.conf /etc/openldap/ldap.conf.bk
cp /opt/docker/ldap.conf /etc/openldap/
mv /etc/nslcd.conf /etc/nslcd.conf.bk
cp /opt/docker/nslcd.conf /etc/
mv /etc/nsswitch.conf /etc/nsswitch.conf.bk
cp /opt/docker/nsswitch.conf /etc/

# encenem servidors nscd i nslcd
/sbin/nscd
/sbin/nslcd

# system-auth
mv /etc/pam.d/system-auth /etc/pam.d/system-auth.bk
cp /opt/docker/system-auth /etc/pam.d/

/bin/bash
