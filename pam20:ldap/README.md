PAM20:LDAP

Host pam per practicar *PAM* amb autenticació local unix(*pam_unix.so*) i autenticació LDAP(*pam_ldap*). Utilitza el paquet nss-pam-ldapd. 
Cal configurar: *ldap.conf*,*nslcd*,*nscd*,*nsswitch.conf*.

Cal usar --privileged al cobntainer per poder fer els muntatges nfs.

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:base
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisix --privileged -d andreupasalamar/pam20:ldap
