PASOS A SEGUIR

1. dnf install nss-pam-ldapd
2. ldap.conf : 		dc=edt,dc=org
	       		ldap://ldap.edt.org
3. /etc/nscd.conf --> tal qual com esta
4. /etc/nslcd.conf :	uri ldap://ldap.edt.org
			base dc=edt,dc=org
5. /etc/nsswitch.conf:	passwd : files ldap systemd
			shadow : files ldap
			group :  files ldap systemd
6. 	/sbin/nscd
	/sbin/nslcd
7. /etc/pam.d/system-auth
	auth        required      pam_env.so
auth        sufficient    pam_unix.so try_first_pass nullok
auth        sufficient    pam_ldap.so
auth        required      pam_deny.so

account     sufficient      pam_unix.so
account     required        pam_ldap.so

password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=
password    sufficient    pam_unix.so try_first_pass use_authtok nullok sha512 shadow
password    sufficient    pam_ldap.so
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
-session     optional      pam_systemd.so
session	    optional	   pam_mkhomedir.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     sufficient      pam_unix.so
session     required        pam_ldap.so

8. Els usuaris ldap no tenen home i com no tenen home s'ha de crear amb el pam_mkhomedir.so
