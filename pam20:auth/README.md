VALORS DE CONTROL

- required: si falla retorna error després d'invocar la resta de moduls.
- requisite: si falla retorna error i acaba.
- sufficient: si esta be i tots els required anteriors estan be retorna tot correcte. si falla s'ignora.
- optional: si nomes esta aquest modul importa si va be o no.
